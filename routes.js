FlowRouter.route( '/terms', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'termsOfService' } ); 
  },
  name: 'termsOfService'
});


FlowRouter.route( '/about', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'about' } ); 
  },
  name: 'about'
});

FlowRouter.route( '/todo', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'todo' } ); 
  },
  name: 'todo'
});


var campaigns = FlowRouter.group({
  prefix: '/campaigns'
});

campaigns.route( '/', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'campaign_list' } ); 
  },
  name: 'campaigns'
});

campaigns.route( '/:_id', {
  action: function() {
    console.log( "We're viewing a single campaign." );
    BlazeLayout.render( 'applicationLayout', { main: 'campaign' } ); 
  }
});