
import { Template } from 'meteor/templating';

import { Campaigns } from '../../models/campaigns.js';

import './campaign_list.html';

 

Template.campaign_list.helpers({

  campaigns() {
    return Campaigns.find({});
  },

});

Template.campaign_list.events({
  'submit .new-campaign'(event) {
  	console.log('test');
  	event.preventDefault();
  	const target = event.target;
    const title = target.title.value;
	const name = target.name.value;
	const user = target.user.value;    
	const amount = target.amount.value;
	const goal = target.goal.value;

    Campaigns.insert({
    	title,
    	name,
    	user,
    	amount,
    	goal,
    	createdAt: new Date()
    });
  },
});

